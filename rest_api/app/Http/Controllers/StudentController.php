<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;

class StudentController extends Controller
{
    public function index()
    {
        return Student::all();
    }

    public function show(Student $student)
    {
        return $student;
    }

    public function store(Request $request)
    {
        $article = Student::create($request->all());

        return response()->json($article, 201);
    }

    public function update(Request $request, Student $article)
    {
        $article->update($request->all());

        return response()->json($article, 200);
    }

    public function delete(Student $article)
    {
        $article->delete();

        return response()->json(null, 204);
    }
}
