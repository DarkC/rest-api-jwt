<?php

use Illuminate\Database\Seeder;
use App\Student;

class StudentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Student::truncate();

        $faker = \Faker\Factory::create();

        // Let's make sure everyone has the same password and 
        // let's hash it before the loop, or else our seeder 
        // will be too slow.
        $curse = Hash::make('isw');
        

      

        // And now let's generate a few dozen users for our app:
        for ($i = 0; $i < 10; $i++) {
            Student::create([
                'name' => $faker->name,
                'curse' => $curse,
            ]);
        }
    }
}
