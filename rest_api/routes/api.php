<?php

use Illuminate\Http\Request;
Use App\Student;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('register', 'UserController@register');
Route::post('login', 'UserController@authenticate');
Route::get('open', 'DataController@open');

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('user', 'UserController@getAuthenticatedUser');
    Route::get('closed', 'DataController@closed');
});

/*
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');
Route::post('register', 'Auth\RegisterController@register');

Route::group(['middleware' => 'auth:api'], function() {
    Route::get('student', 'StudentController@index');
    Route::get('student/{student}', 'StudentController@show');
    Route::post('student', 'StudentController@store');
    Route::put('student/{student}', 'StudentController@update');
    Route::delete('student/{student}', 'StudentController@delete');

    if (Auth::check()) {
    	$message = [
        "error" => [
            "code" => 401,
            "message" => "Invalid Credentials"
        ]
    	];
        return Response::json($message, 401);
    }
});
*/